import express from 'express';
import gameController from './controller';

const router = new express.Router();

router.get('', (req, res) => {
  gameController.getAll(req, res);
});

router.post('', (req, res) => {
  gameController.create(req, res);
});

router.put('/:id/:status', (req, res) => {
  gameController.changeStatus(req, res);
});

export default router;
