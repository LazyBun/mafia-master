import mongoose from 'mongoose';

const GameSchema = new mongoose.Schema({
  creator: { type: String, required: true, unique: false, index: false },
  gameId: { type: String, required: true, unique: false, index: true },
  status: { type: String, required: true, unique: false, index: false },
}, { collection: 'Game' });

const GameModel = mongoose.model('Game', GameSchema);

GameModel.getAll = () => {
  return GameModel.find({});
};

GameModel.new = (gameToCreate) => {
  return gameToCreate.save();
};

GameModel.findSingle = (params) => {
  return GameModel.findOne(params).exec(handleResponse);
};

GameModel.changeStatus = (id, status) => {
  return GameModel.findByIdAndUpdate(id, { $set: { status } }, { new: true }, handleResponse);
};

let handleResponse = (err, item) => {
  if (err) return null;
  return item;
};

GameModel.GameState = {
  STARTED: 'started',
  EXPIRED: 'expired',
};

export default GameModel;
