import Game from './model';
import logger from '../core/logger/app-logger';
import { random } from '../utils';

const controller = {};

controller.getAll = async (req, res) => {
  try {
    logger.debug('Getting all games');
    const games = await Game.getAll();
    logger.info('Got all games');
    res.send(games);
  } catch (err) {
    logger.error(`Error while getting games - ${err}`);
    res.status(500).send('Error getting all games');
  }
};

controller.create = async (req, res) => {
  try {
    logger.debug('Adding game...');
    if (!req.body.creator) throw new TypeError('Bad body');

    logger.debug('Generating game id');
    let gameId = random.word({ length: 4 });
    while (await Game.findSingle({ gameId, status: { $ne: Game.GameState.STARTED } })) {
      logger.debug('Game id collision, regenerating');
      gameId = random.word({ length: 4 });
    }
    logger.debug('Generated game id');

    const gameToCreate = new Game({
      creator: req.body.creator,
      // eslint-disable-next-line object-shorthand
      gameId: gameId,
      status: Game.GameState.STARTED,
    });

    const createdGame = await Game.new(gameToCreate);
    logger.info('Added game');
    res.send(`added: ${createdGame}`);
  } catch (err) {
    logger.error(`Error while adding a game - ${err}`);
    res.status(500).send('Error adding a game');
  }
};

controller.changeStatus = async (req, res) => {
  try {
    if (!req.params || !req.params.id || !Game.GameState[req.params.status]) throw new TypeError('Bad params');
    logger.debug('Changing game status');
    const game = await Game.changeStatus(req.params.id, Game.GameState[req.params.status]);
    logger.info('Game status changed');
    res.send(game);
  } catch (err) {
    logger.error(`Error while changing game status - ${err}`);
    res.status(500).send('Error changing game status');
  }
};


export default controller;
