import Game from './model';


export default (socket) => {
  socket.on('join', async (data) => {
    if (data &&
        data.gameId &&
        data.name &&
        (await Game.findSingle({ gameId: data.gameId, status: { $ne: Game.GameState.EXPIRED } }))) {
      socket.join(data.gameId, () => {

        const rooms = Object.keys(socket.rooms);
        console.log(rooms);

        socket.to(data.gameId).emit('message', `${data.name} joined the game.`);
      });
    }
  });
};
