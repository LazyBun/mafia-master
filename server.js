import http from 'http';
import SocketIO from 'socket.io';
import morgan from 'morgan';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import logger from './core/logger/app-logger';
import config from './core/config/config.dev';
import connectToDb from './db/connect';

import game from './game/route';
import gameWs from './game/gameWs';

const port = config.serverPort;
logger.stream = {
// eslint-disable-next-line no-unused-vars
  write(message, encoding) {
    logger.info(message);
  },
};

connectToDb();

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev', { stream: logger.stream }));

app.use('/game', game);

// Index route
app.get('/', (req, res) => {
  res.send('Invalid endpoint!');
});

app.listen(port, () => {
  logger.info('server started - ', port);
});


const server = new http.Server(app);
server.listen(1337, () => {
  console.log('Listening on 1337');
});

const io = new SocketIO(server);

io.on('connection', (socket) => {
  console.log(`User:${socket.id} connected.`);
});

io.of('game')
  .on('connection', gameWs);
